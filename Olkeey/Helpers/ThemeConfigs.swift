//
//  ThemeConfigs.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 26/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class Config {
    static public let themeColor = UIColor.init(red: 255 / 255, green: 68 / 255, blue: 16 / 255, alpha: 1)
}

extension UIColor {
    static var olkeeyOrange = UIColor(red: 1, green: 40/255, blue: 28/255, alpha: 1)
}

extension UIColor {
    static var olkeeyOrangeLight = UIColor(red: 1, green: 40/255, blue: 28/255, alpha: 0.3)
}
