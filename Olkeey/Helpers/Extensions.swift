//
//  Extensions.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 25/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

extension UILabel {
    convenience init(text: String, font: UIFont, color: UIColor, numberOfLines: Int = 1) {
        self.init(frame: .zero)
        self.text = text
        self.font = font
        self.textColor = color
        self.numberOfLines = numberOfLines
    }
}

extension UIImageView {
    convenience init(cornerRadius: CGFloat) {
        self.init(image: nil)
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFill
    }
}

extension UIButton {
    convenience init(title: String) {
        self.init(type: .system)
        self.setTitle(title, for: .normal)
        self.setTitleColor(UIColor.init(red: 255 / 255, green: 68 / 255, blue: 14 / 255, alpha: 1), for: .normal)
    }
}

struct AnchoredConstraints {
    var top, trailing, bottom, leading, width, height: NSLayoutConstraint?
}


