//
//  PanphletItem.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 29/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

struct PanphletItem {
    
    let category: String
    let title: String
    let image: UIImage
    let description: String
    let backgroundColor: UIColor
    
    let cellType: CellType
    
    let apps: [FeedResult]
    
    enum CellType: String {
        case single, multiple
    }
}
