//
//  BackCustomBehaviorNavigationController.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 02/10/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class BackCustomNavigationController: UINavigationController, UIGestureRecognizerDelegate {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.interactivePopGestureRecognizer?.delegate = self
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return self.viewControllers.count > 1
    }
}
