//
//  TodayCollectionViewController.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 27/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class TodayCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {

    
    
    // the items are initiated to be only filled in the dispatchgroups
    var items = [PanphletItem]()
    
    var topGrossingAppGroup: HomeModels?
    var gamesAppGroup: HomeModels?
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.color = .darkGray
        activityIndicatorView.startAnimating()
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.superview?.setNeedsLayout()
        
    }
    
    let blurVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(blurVisualEffectView)
        blurVisualEffectView.anchor(top: view.topAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, leading: view.leadingAnchor)
        blurVisualEffectView.alpha = 0
        
        view.addSubview(activityIndicatorView)
        activityIndicatorView.anchor(top: view.topAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, leading: view.leadingAnchor)
        fetchData()
        
        collectionView.backgroundColor = UIColor.init(red: 244 / 255, green: 244 / 255, blue: 250 / 255, alpha: 1)
        navigationItem.title = "Today"
        navigationController?.navigationBar.tintColor = Config.themeColor
        navigationController?.navigationBar.prefersLargeTitles = true
        collectionView.alwaysBounceVertical = true
        
        navigationController?.isNavigationBarHidden = true
        
        
        collectionView.contentInset = .init(top: 32, left: 0, bottom: 32, right: 0)
        collectionView.register(PanfletColletionViewCell.self, forCellWithReuseIdentifier: PanphletItem.CellType.single.rawValue)
        
        // This is for the additional types of cells
        collectionView.register(PanphletMultipleAppCell.self, forCellWithReuseIdentifier: PanphletItem.CellType.multiple.rawValue)
       


    }
    
    fileprivate func fetchData() {
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        NetworkingService.shared.fetchTopFreeApps { (appGroup, err) in
            if let err = err {
                print(err)
                return
            }
            self.topGrossingAppGroup = appGroup
            dispatchGroup.leave()
        }
        dispatchGroup.enter()
        NetworkingService.shared.fetchGames { (appGroup, err) in
            if let err = err {
                print(err)
                return
            }
            self.gamesAppGroup = appGroup
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            self.activityIndicatorView.stopAnimating()
            self.items = [
                PanphletItem.init(category: "Daily List", title: self.topGrossingAppGroup?.feed.title ?? "", image: #imageLiteral(resourceName: "garageband"), description: "", backgroundColor: .white, cellType: .multiple, apps: self.topGrossingAppGroup?.feed.results ?? []),
                PanphletItem.init(category: "Rock", title: "The Guitars", image: #imageLiteral(resourceName: "garageband"), description: "Rock is pumping the heart of the people who pledged the true respect for the ritual!", backgroundColor: .white, cellType: .single, apps: []),
                PanphletItem.init(category: "Axe", title: "Bell And More", image: #imageLiteral(resourceName: "bell"), description: "The Bahian rithm is making people from all around the world take a time to dance and sing a little right!", backgroundColor: .white, cellType: .single, apps: []),
                PanphletItem.init(category: "Daily List", title: self.gamesAppGroup?.feed.title ?? "", image: #imageLiteral(resourceName: "garageband"), description: "", backgroundColor: .white, cellType: .multiple, apps: self.gamesAppGroup?.feed.results ?? [])
            ]
            self.collectionView.reloadData()
        }
//        NetworkingService.shared.fetchGames { (appGroup, err) in
//
//            self.results = appGroup?.feed.results ?? []
//            DispatchQueue.main.async {
//                self.collectionView.reloadData()
//            }
//        }
    }
    
    private func setupBlur() {
        
    }
    
    var detailedTableViewController: DetailedFullScreenTableViewController!
    var topAnchor: NSLayoutConstraint?
    var leadingAnchor: NSLayoutConstraint?
    var widthAnchor: NSLayoutConstraint?
    var heightAnchor: NSLayoutConstraint?
    
    fileprivate func showDailyListFullScreen(_ indexPath: IndexPath) {
        let rankingTableListController = PanphletMultipleAppCellController(mode: .fullscreen)
        rankingTableListController.results = self.items[indexPath.item].apps
        // important to pass in a navigation controller with this controller embbeded otherwise data wont flow correctly
        present(BackCustomNavigationController(rootViewController: rankingTableListController), animated: true)
    }
    
    fileprivate func setupSingleAppFullScreenController(_ indexPath: IndexPath) {
        let detailedTableViewController = DetailedFullScreenTableViewController()
        detailedTableViewController.panphletItem = self.items[indexPath.row]
        detailedTableViewController.tableView.reloadData()
        detailedTableViewController.dismissHandler = {
            self.handleRemoveDetailedFullScreen()
        }
        detailedTableViewController.view.layer.cornerRadius = 16
        self.detailedTableViewController = detailedTableViewController
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(handleDrag))
        gesture.delegate = self
        detailedTableViewController.view.addGestureRecognizer(gesture)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    var detailedTableViewOffsetStart: CGFloat = 0
    
    @objc
    fileprivate func handleDrag(gesture: UIPanGestureRecognizer) {
        if gesture.state == .began {
            detailedTableViewOffsetStart = detailedTableViewController.tableView.contentOffset.y
        }
        
        let translationY = gesture.translation(in: detailedTableViewController.view).y
        self.blurVisualEffectView.alpha = 1
        
        if detailedTableViewController.tableView.contentOffset.y > 0 {
            return
        }
        if gesture.state == .changed {
            
            if translationY > 0 {
                let trueOffset = translationY - detailedTableViewOffsetStart
                
                var scale = 1 - trueOffset / 1000
                scale = min(1, scale)
                scale = max(0.6, scale)
                let transform: CGAffineTransform = .init(scaleX: scale, y: scale)
                self.detailedTableViewController.view.transform = transform
            }
        }
        else if gesture.state == .ended {
            if translationY > 0 {
                handleRemoveDetailedFullScreen()
            }
        }
    }
    
    fileprivate func setupStartingFrame(_ indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) else { return }
        
        guard let startingFrame = cell.superview?.convert(cell.frame, to: nil) else { return }
        self.startingFrame = startingFrame
    }
    
    fileprivate func setupAppFullScreenStartingFrame(_ indexPath: IndexPath) {
        guard let detailedTableView = self.detailedTableViewController.view else { return }
        view.addSubview(detailedTableView)
        addChild(self.detailedTableViewController)
        
        
        self.collectionView.isUserInteractionEnabled = false
        
        setupStartingFrame(indexPath)
        
        guard let startingFrame = self.startingFrame else { return }
        
        detailedTableView.translatesAutoresizingMaskIntoConstraints = false
        topAnchor = detailedTableView.topAnchor.constraint(equalTo: view.topAnchor, constant: startingFrame.origin.y)
        
        leadingAnchor = detailedTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: startingFrame.origin.x)
        widthAnchor = detailedTableView.widthAnchor.constraint(equalToConstant: startingFrame.width)
        heightAnchor = detailedTableView.heightAnchor.constraint(equalToConstant: startingFrame.height)
        
        [self.topAnchor, self.leadingAnchor, self.widthAnchor, self.widthAnchor, heightAnchor].forEach({$0?.isActive = true})
        // detailedTableView.frame = startingFrame
        self.view.layoutIfNeeded()
    }
    var anchoredConstraints: AnchoredConstraints?
    
    fileprivate func beginFullScreenAnimation() {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            // detailedTableView.frame = self.view.frame
            
            
            self.topAnchor?.constant = 0
            self.leadingAnchor?.constant = 0
            self.heightAnchor?.constant = self.view.frame.height
            self.widthAnchor?.constant = self.view.frame.width
            
            self.view.layoutIfNeeded() // liberates all the old constraints that can block the animation
            
            self.tabBarController?.tabBar.transform = CGAffineTransform(translationX: 0, y: 100)
            
            guard let cell = self.detailedTableViewController.tableView.cellForRow(at: [0, 0]) as? PanfletFullScreenHeaderCell else { return }
            cell.panfletCollectionViewCell.topConstraint.constant = 48
            cell.layoutIfNeeded()
        }, completion: nil)
    }
    
    fileprivate func showSingleAppFullScreen(indexPath: IndexPath) {
        
        setupSingleAppFullScreenController(indexPath)
        
        setupAppFullScreenStartingFrame(indexPath)
     
        beginFullScreenAnimation()
        }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch items[indexPath.item].cellType {
        case .multiple:
            showDailyListFullScreen(indexPath)
        default:
            showSingleAppFullScreen(indexPath: indexPath)
        }
    }
    
    var startingFrame: CGRect?
    
    @objc func handleRemoveDetailedFullScreen() {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            
            self.blurVisualEffectView.alpha = 0
            self.detailedTableViewController.view.transform = .identity
            
            self.detailedTableViewController.tableView.contentOffset = .zero
            
            guard let startingFrame = self.startingFrame else { return }
            
            self.topAnchor?.constant = startingFrame.origin.y
            self.leadingAnchor?.constant = startingFrame.origin.x
            self.heightAnchor?.constant = startingFrame.height
            self.widthAnchor?.constant = startingFrame.width
            
            self.view.layoutIfNeeded()
            //gesture.view?.frame = self.startingFrame ?? .zero
            self.tabBarController?.tabBar.transform = .identity
            
            guard let cell = self.detailedTableViewController.tableView.cellForRow(at: [0, 0]) as? PanfletFullScreenHeaderCell else { return }
          
            self.detailedTableViewController.closeButton.alpha = 0
            cell.panfletCollectionViewCell.topConstraint.constant = 24
            cell.layoutIfNeeded()
            
            
        }, completion: { _ in
            self.view.layoutIfNeeded()
            
            if let detailedView = self.detailedTableViewController {
                detailedView.view.removeFromSuperview()
                detailedView.removeFromParent()
                self.collectionView.isUserInteractionEnabled = true
                self.view.layoutIfNeeded()
            }
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 32
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 64, height: 480)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellType = items[indexPath.item].cellType.rawValue
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType, for: indexPath) as! TemplatePanphletCell
        // THIS IS AN EXAMPLE TO ADD BLUR TO IMAGES THAT RENDER EVENTS FULL PICTURES AND ARTIST PERFORMANCE
//        let blurEffect = UIBlurEffect(style: .regular)
//        let visualEffectView = UIVisualEffectView(effect: blurEffect)
//        cell.addSubview(visualEffectView)
//        visualEffectView.anchor(top: cell.topAnchor, trailing: cell.trailingAnchor, bottom: nil, leading: cell.leadingAnchor,padding: .zero)
//        visualEffectView.heightAnchor.constraint(equalToConstant: 100).isActive = true
//        cell.clipsToBounds = true
        cell.panphletItem = items[indexPath.item]
        
        (cell as? PanphletMultipleAppCell)?.multipleAppsController.collectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleAppListTap)))
        return cell
//        if indexPath.item == 0 {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: multipleCellid, for: indexPath) as! PanphletMultipleAppCell
//            cell.panphletItem = items[indexPath.item]
//
//            return cell
//        }
//
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PanfletColletionViewCell
//        cell.panphletItem = items[indexPath.item]
//        return cell
    }
    
    
    @objc private func handleAppListTap(gesture: UIGestureRecognizer) {
        let collectionView = gesture.view
        
        var superview = collectionView?.superview
        
        while superview != nil {
            if let cell = superview as? PanphletMultipleAppCell {
                guard let indexPath = self.collectionView.indexPath(for: cell) else { return }
                let apps = self.items[indexPath.item].apps
                let fullListController = PanphletMultipleAppCellController(mode: .fullscreen)

                fullListController.results = apps
                    present(BackCustomNavigationController(rootViewController: fullListController), animated: true)
                return
            }
            
            superview = superview?.superview
            
        }
        
       
        
    }
}
