//
//  DetailedFullScreenTableViewController.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 29/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class DetailedFullScreenTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.isScrollEnabled = false
            scrollView.isScrollEnabled = true
        }
        
        let translationX = -self.view.frame.width
        let transform = scrollView.contentOffset.y > 100 ? CGAffineTransform(translationX: translationX, y: 0) : .identity
        
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            self.floatingContainerView.transform = transform
        })
       
        
    }
    // WATCH CLOSELY THIS IS AWESOME
    let tableView = UITableView(frame: .zero, style: .plain)
    
    var dismissHandler: (() -> ())?
    var panphletItem: PanphletItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.clipsToBounds = true
        
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, leading: view.leadingAnchor)
        tableView.delegate = self
        tableView.dataSource = self
        
        setupCloseButton()
        
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.contentInsetAdjustmentBehavior = .never
        
        let bottomSafeHeight = UIApplication.shared.statusBarFrame.height
        tableView.contentInset = .init(top: 0, left: 0, bottom: bottomSafeHeight, right: 0)
        
        setupFloatingControls()
    }
    let floatingContainerView = UIView()
    @objc
    fileprivate func handleAppearOfAppTeaser() {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            self.floatingContainerView.transform = .init(translationX: -self.view.frame.width, y: 0)
        })
    }
    
    fileprivate func setupFloatingControls() {
        
        //floatingContainerView.backgroundColor = .red
        view.addSubview(floatingContainerView)
        let bottomPadding = UIApplication.shared.statusBarFrame.height
        floatingContainerView.anchor(top: nil, trailing: view.trailingAnchor, bottom: view.bottomAnchor, leading: nil, padding: .init(top: 0, left: 0, bottom: -bottomPadding, right: -view.frame.width - 40), size: .init(width: view.frame.width - 40, height: 80))
        floatingContainerView.layer.cornerRadius = 50
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleAppearOfAppTeaser)))
        
        let blurVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        floatingContainerView.addSubview(blurVisualEffectView)
        blurVisualEffectView.anchor(top: floatingContainerView.topAnchor, trailing: floatingContainerView.trailingAnchor, bottom: floatingContainerView.bottomAnchor, leading: floatingContainerView.leadingAnchor)
        floatingContainerView.clipsToBounds = true
        
        let imageView = UIImageView(cornerRadius: 16)
        imageView.image = panphletItem?.image
       
        imageView.heightAnchor.constraint(equalToConstant: 56).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 56).isActive = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        
        let labelContainer = VerticalStackView(arrangedSubviews: [UILabel(text: "Rock", font: .boldSystemFont(ofSize: 16), color: .black), UILabel(text: "Let' Play it", font: .systemFont(ofSize: 14), color: .black)])
        
        
        let getButton = UIButton(title: "Contract")
        getButton.titleLabel?.font = .boldSystemFont(ofSize: 14)
        getButton.setTitleColor(.white, for: .normal)
        getButton.backgroundColor = .gray
        getButton.layer.cornerRadius = 16
        getButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        getButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        getButton.translatesAutoresizingMaskIntoConstraints = false
        
        let stackView = UIStackView(arrangedSubviews: [imageView, labelContainer, getButton ], customSpacing: 16)
        floatingContainerView.addSubview(stackView)
        
        stackView.anchor(top: floatingContainerView.topAnchor, trailing: floatingContainerView.trailingAnchor, bottom: floatingContainerView.bottomAnchor, leading: floatingContainerView.leadingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 52))
        stackView.alignment = .center
    }
    
    let closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "closeX"), for: .normal)
        button.tintColor = Config.themeColor
        return button
    }()
    
    fileprivate func setupCloseButton() {
       view.addSubview(closeButton)
        closeButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, bottom: nil, leading: nil, padding: .init(top: 12, left: 0, bottom: 0, right: 12), size: .init(width: 30, height: 30))
        closeButton.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)

    }
    
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let header = PanfletColletionViewCell()
//        return header
//    }
//
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 400
//    }
//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    // VERY PROBLEMATIC VERY PROBLEMATIC
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0 {
            let headerCell = PanfletFullScreenHeaderCell()
            headerCell.panfletCollectionViewCell.panphletItem = panphletItem
            headerCell.panfletCollectionViewCell.layer.cornerRadius = 0
            headerCell.clipsToBounds = true
            // not really tested
            headerCell.panfletCollectionViewCell.backgroundView = nil
            return headerCell
        }
        let cell = DetailedFullScreenDescriptionCell()
        return cell
    }
    
    @objc func handleDismiss(button:UIButton) {
        button.isHidden = true
        dismissHandler?()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 480
        }
        
        return UITableView.automaticDimension
    }
    
    
}
