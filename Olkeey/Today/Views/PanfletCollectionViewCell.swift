//
//  PanfletCollectionViewCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 27/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class PanfletColletionViewCell: TemplatePanphletCell {
    
    override var panphletItem: PanphletItem! {
        didSet {
            categoryLabel.text = panphletItem.category
            titleLabel.text = panphletItem.title
            imageView.image = panphletItem.image
            descriptionText.text = panphletItem.description
            backgroundColor = panphletItem.backgroundColor
            backgroundView?.backgroundColor = panphletItem.backgroundColor
            
        }
    }
    
    let categoryLabel = UILabel(text: "Johnnie's Guitars", font: .boldSystemFont(ofSize: 18), color: .black)
    let titleLabel = UILabel(text: "That sweet wip!", font: .boldSystemFont(ofSize: 28), color: .black)
    
    let imageView = UIImageView(image: #imageLiteral(resourceName: "garageband"))
    
    let descriptionText = UILabel(text: "The Best Package for Shows that you get, all the greatest artists are hear for you to choose", font: .systemFont(ofSize: 12), color: .black, numberOfLines: 0)

    var topConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.init(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1)
        layer.cornerRadius = 16
        
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        
        
        let imageViewContainer = UIView()
        
        imageViewContainer.addSubview(imageView)
        imageViewContainer.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: imageViewContainer, attribute: .centerX, multiplier: 1, constant: 0))
         imageViewContainer.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: imageViewContainer, attribute: .centerY, multiplier: 1, constant: 0))
        imageView.widthAnchor.constraint(equalToConstant: 260).isActive = true
        imageViewContainer.heightAnchor.constraint(equalToConstant: 260).isActive = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        
        let labelsStackView = VerticalStackView(arrangedSubviews: [categoryLabel, titleLabel])
        
        
        // REVIEW THIS CODE BECAUSE PROBABLY THE TITLE AND SUBTITLE WILL BE NEDDED TO BE SET APART
        let verticalStackView = VerticalStackView(arrangedSubviews: [labelsStackView, imageViewContainer, descriptionText])
        verticalStackView.distribution = .equalSpacing
        addSubview(verticalStackView)
       
        verticalStackView.anchor(top: nil, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor, padding: .init(top: 0, left: 24, bottom: -24, right: 24))
        self.topConstraint = verticalStackView.topAnchor.constraint(equalTo: topAnchor, constant: 24)
        self.topConstraint.isActive = true
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
//        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
//        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
//        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
