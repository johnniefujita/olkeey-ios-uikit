//
//  PanfletCollectionViewHeaderCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 29/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit


class PanfletFullScreenHeaderCell: UITableViewCell {
    
    let panfletCollectionViewCell = PanfletColletionViewCell()
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(panfletCollectionViewCell)
        //addSubview(closeButton)
        
        
        panfletCollectionViewCell.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
        //closeButton.anchor(top: topAnchor, trailing: trailingAnchor, bottom: nil, leading: nil, padding: .init(top: 36, left: 0, bottom: 0, right: 24), size: .init(width: 30, height: 30))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
