//
//  TemplatePanphletCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 30/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class TemplatePanphletCell: UICollectionViewCell {
    // this is to receive the model object to when a fetch occurs loads the json data into it
    var panphletItem: PanphletItem!
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.transform = .init(scaleX: 0.95, y: 0.95)
                })
            }
            else {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.transform = .identity
                })
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundView = UIView()
        self.backgroundView?.backgroundColor = .white
        addSubview(self.backgroundView!)
        
        self.backgroundView?.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
        self.backgroundView?.layer.shadowRadius = 10
        self.backgroundView?.layer.shadowColor = UIColor.black.cgColor
        self.backgroundView?.layer.shadowOpacity = 0.1
        self.backgroundView?.layer.cornerRadius = 16
        self.backgroundView?.layer.shadowOffset = .init(width: 0, height: 0)
        self.backgroundView?.layer.shouldRasterize = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
