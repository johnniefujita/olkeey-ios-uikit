//
//  MultipleAppCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 29/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class PanphletMultipleAppCell: TemplatePanphletCell {
    
    override var panphletItem: PanphletItem! {
        didSet {
            categoryLabel.text = panphletItem.category
            titleLabel.text = panphletItem.title
            
            multipleAppsController.results = panphletItem.apps
            multipleAppsController.collectionView.reloadData()
        }
    }
    
    let categoryLabel = UILabel(text: "Apps", font: .boldSystemFont(ofSize: 20), color: .black)
    let titleLabel = UILabel(text: "Kings of the Hill", font: .boldSystemFont(ofSize: 32), color: .black)
    
    let multipleAppsController = PanphletMultipleAppCellController(mode: .small)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 16
        
        
        
        let labelsStackView = VerticalStackView(arrangedSubviews: [categoryLabel, titleLabel])
        
        let verticalStackView = VerticalStackView(arrangedSubviews: [labelsStackView, multipleAppsController.view], spacing: 12)
        titleLabel.numberOfLines = 2
        addSubview(verticalStackView)
        verticalStackView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor, padding: .init(top: 24, left: 24, bottom: -24, right: 24))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

