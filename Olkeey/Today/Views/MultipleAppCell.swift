//
//  MultipleAppCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 01/10/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit
import SDWebImage

class MultipleAppCell: UICollectionViewCell {
    
    var app: FeedResult! {
        didSet {
            nameLabel.text = app.name
            companyLabel.text = app.artistName
            imageView.sd_setImage(with: URL(string: app.artworkUrl100))
        }
    }
    
    let imageView = UIImageView(cornerRadius: 12)
    let nameLabel = UILabel(text: "App", font: .systemFont(ofSize: 20), color: .black)
    let companyLabel = UILabel(text: "CompanyLabel", font: .systemFont(ofSize: 14), color: .lightGray)
    let contractButton = UIButton(title: "contract")
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.88, alpha: 0.5)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contractButton.backgroundColor = UIColor(white: 0.95, alpha: 1)
        contractButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        contractButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        contractButton.layer.cornerRadius = 16
        contractButton.titleLabel?.font = .boldSystemFont(ofSize: 14)
        
        imageView.backgroundColor = .purple
        imageView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        
        let labelStack = UIStackView(arrangedSubviews: [nameLabel, companyLabel])
        labelStack.axis = .vertical
        
        
        let stackView = UIStackView(arrangedSubviews: [imageView, labelStack, contractButton])
        addSubview(stackView)
        stackView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
        stackView.spacing = 16
        stackView.alignment = .center
        
        addSubview(separatorView)
        separatorView.anchor(top: nil, trailing: trailingAnchor, bottom: bottomAnchor, leading: nameLabel.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 6, right: 0), size: .init(width: 0, height: 0.5))
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
