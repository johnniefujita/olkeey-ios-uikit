//
//  PageCellViewController.swift
//  Introductions_Olkeey
//
//  Created by johnnie fujita on 01/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class PageCollectionViewCell: UICollectionViewCell {
    
    var page: ModelPage? {
        didSet {
            guard let unwrappedPage = page else { return }
            
            logoImageView.image = UIImage(named: unwrappedPage.imageName)
            headlineTextView.text = NSLocalizedString(unwrappedPage.headlineText, comment: "")
        }
    }
    
    
    private let previousButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(NSLocalizedString("back", comment: "back"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(NSLocalizedString("next", comment: "next"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 4
        pc.pageIndicatorTintColor = .olkeeyOrangeLight
        pc.currentPageIndicatorTintColor = .olkeeyOrange
        return pc
    }()
    
    
    private let logoImageView: UIImageView = {
        var imageView = UIImageView(image: UIImage(named: "olkeey_white_vertical"))
        imageView.contentMode = .scaleAspectFit
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private let headlineTextView: UITextView = {
        let textView = UITextView()
        textView.text = NSLocalizedString("build experiences", comment: "build")
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = UIFont.boldSystemFont(ofSize: 22)
        textView.isEditable = false
        textView.isSelectable = false
        textView.isScrollEnabled = false
        textView.textAlignment = .left
        //textView.textColor = UIColor.init(red: 1, green: (40/255), blue: (28/255), alpha: 1)
        
        return textView
    }()
    
    private let descriptionTextView: UITextView = {
        let textView = UITextView()
        
        textView.text = NSLocalizedString("build experiences description", comment: "build description") + "jojojojojo"
        
        
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.textColor = UIColor.darkGray
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isSelectable = false
        textView.isEditable = false
        
        textView.textAlignment = .left
        
        return textView
    }()
    
    
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        
        setupBottomLayout()
        
        
    }
    
    
    private func setupLayout() {
        addSubview(logoImageView)
        addSubview(headlineTextView)
        addSubview(descriptionTextView)
        
        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 50).isActive = true
        logoImageView.widthAnchor.constraint(equalTo: heightAnchor, multiplier: 0.2).isActive = true
        logoImageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.2).isActive = true
        
       
        headlineTextView.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 30).isActive = true
        headlineTextView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        headlineTextView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
      
        
        descriptionTextView.topAnchor.constraint(equalTo: headlineTextView.bottomAnchor, constant: 0).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
        
    }
    
    private func setupBottomLayout(){
        // view.addSubview(previousButton)
        //previousButton.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        


        let bottomCtlStackViews = UIStackView(arrangedSubviews:
            [pageControl])
        
        bottomCtlStackViews.distribution = .fillEqually
        
        addSubview(bottomCtlStackViews)
       
        bottomCtlStackViews.anchor(top: nil, trailing: trailingAnchor, bottom:  bottomAnchor, leading: leadingAnchor, padding: .init(top: 0, left: 0, bottom: -120, right: 0))
        
    }
    
    
    // this is a code check ensured by the compiler, otherwise it will not compile
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
