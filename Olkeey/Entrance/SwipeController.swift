//
//  SwipeController.swift
//  Introductions_Olkeey
//
//  Created by johnnie fujita on 01/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class SwipeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    
    
    
    
    
    let pages = [
        ModelPage(imageName: "olkeey_white_vertical", headlineText: NSLocalizedString("build experiences", comment: "build description")),
        ModelPage(imageName: "olkeey_white_horizontal", headlineText: NSLocalizedString("virtualized events", comment: "build description")),
        ModelPage(imageName: "olkeey_white_vertical", headlineText: NSLocalizedString("engage with performers", comment: "interact with the performers")),
        ModelPage(imageName: "olkeey_white_vertical", headlineText: NSLocalizedString("social life", comment: "social life")),
        ModelPage(imageName: "olkeey_white_vertical", headlineText: NSLocalizedString("immersive adventures", comment: "immersive adventures"))
    ]
    
    let imageNames = ["olkeey_white_vertical", "olkeey_white_horizontal", "Asset9"]
    let headlines = [NSLocalizedString("build experiences", comment: "build description"), NSLocalizedString("virtualized events", comment: "virtualized events"), NSLocalizedString("interact with performers", comment: "interact with artists")]
    
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = Config.themeColor
        button.setTitle("Login", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(loginSubmit), for: .touchUpInside)
        return button
    }()
    
    @objc
    fileprivate func loginSubmit() {
        let tabBarController = BaseTabBarController()
        tabBarController.tabBar.isTranslucent = false
        navigationController?.pushViewController(tabBarController, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        collectionView?.backgroundColor = .white
        
        // this collection view cell is not customizable enough
        collectionView?.register(PageCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        
        collectionView?.isPagingEnabled = true
        view.addSubview(loginButton)
        loginButton.anchor(top: nil, trailing: view.trailingAnchor, bottom: view.bottomAnchor, leading: view.leadingAnchor, padding: .init(top: 0, left: 20, bottom: -40, right: 20), size: .init(width: 0, height: 60))
        loginButton.layer.cornerRadius = 20
        loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        self.buttonTopContraint = 120
    }
    
    var buttonTopContraint: CGFloat?
    
    @objc
    fileprivate func login() {
        let homeNavigation = BaseTabBarController()
        navigationController?.pushViewController(homeNavigation, animated: true)    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) ->  Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! PageCollectionViewCell

        // cell.backgroundColor = indexPath.item % 2 == 0 ? .green : .white
       
        
        /*
        let imageName = pages[indexPath.item].imageName
        cell.logoImageView.image = UIImage(named: imageName)
        cell.headlineTextView.text = pages[indexPath.item].headlineText
        */
        
        let page = pages[indexPath.item]
        cell.page = page
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
}
