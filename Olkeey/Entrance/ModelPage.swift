//
//  ModelPage.swift
//  Introductions_Olkeey
//
//  Created by johnnie fujita on 01/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import Foundation


struct ModelPage {
    
    let imageName: String
    let headlineText: String
    //let descriptionText: String
}
