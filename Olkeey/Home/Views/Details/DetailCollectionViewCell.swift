//
//  DetailCollectionViewCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 26/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit
import SDWebImage

class DetailCollectionViewCell: UICollectionViewCell {
    var app:Result! {
        didSet {
            nameLabel.text = app?.trackName
            releaseNotesLabel.text = app?.releaseNotes
            appIconImageView.sd_setImage(with: URL(string: app?.artworkUrl100 ?? ""))
            priceButton.setTitle(app?.formattedPrice ?? "???", for: .normal)
        }
    }
    
    
    let appIconImageView = UIImageView(cornerRadius: 16)
    let nameLabel = UILabel(text: "App Name: WOW", font: .boldSystemFont(ofSize: 20), color: .black, numberOfLines: 2)
    
    let priceButton = UIButton(title: "$4.99")
    let whatsNewLabel = UILabel(text: "What's New", font: .boldSystemFont(ofSize: 18), color: .black)
    
    let releaseNotesLabel = UILabel(text: "Loreism importiaer jfeoa oeirle vaicoei ejfdme mask pytt ieureativivi uimgaerfdae gospas", font: .systemFont(ofSize: 14), color: .black, numberOfLines: 0)
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        //ICON IMAGE CONFIGURE
        appIconImageView.widthAnchor.constraint(equalToConstant: 140).isActive = true
        appIconImageView.heightAnchor.constraint(equalToConstant: 140).isActive = true
        appIconImageView.backgroundColor = .orange
        
        priceButton.backgroundColor = Config.themeColor
        priceButton.heightAnchor.constraint(equalToConstant: 32)
        priceButton.layer.cornerRadius = 16
        priceButton.titleLabel?.font = .boldSystemFont(ofSize: 16)
        priceButton.setTitleColor(.white, for: .normal)
        priceButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        let stackView = VerticalStackView(arrangedSubviews: [UIStackView(arrangedSubviews: [appIconImageView, VerticalStackView(arrangedSubviews: [nameLabel, UIStackView(arrangedSubviews: [priceButton, UIView()]), UIView()], spacing: 16)], customSpacing: 16), whatsNewLabel, releaseNotesLabel], spacing: 16)
        addSubview(stackView)
        stackView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor, padding: .init(top: 16, left: 16, bottom: -16, right: 16))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIStackView {
    convenience init(arrangedSubviews: [UIView], customSpacing: CGFloat = 0) {
        self.init(arrangedSubviews: arrangedSubviews)
        self.spacing = customSpacing
    }
}
