//
//  ReviewCollectionViewCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 26/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class ReviewCollectionViewCell: UICollectionViewCell {
    
    let reviewSubcellsController = ReviewSubcellsController()
    let reviewLabel = UILabel(text: "Review & Rating", font: .boldSystemFont(ofSize: 20), color: .black)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(reviewSubcellsController.view)
        addSubview(reviewLabel)
        reviewLabel.anchor(top: topAnchor, trailing: trailingAnchor, bottom: reviewSubcellsController.view.topAnchor, leading: leadingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        reviewSubcellsController.view.anchor(top: reviewLabel.bottomAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
