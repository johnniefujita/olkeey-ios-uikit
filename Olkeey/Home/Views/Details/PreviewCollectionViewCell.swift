//
//  PreviewCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 26/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class PreviewCollectionViewCell: UICollectionViewCell {
    
    let paginationController = PreviewSubviewsController()
    let previewLabel = UILabel(text: "Preview", font: .boldSystemFont(ofSize: 20), color: .black)
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(paginationController.view)
        addSubview(previewLabel)
        previewLabel.heightAnchor.constraint(equalToConstant: 60)
        previewLabel.anchor(top: topAnchor, trailing: trailingAnchor, bottom: paginationController.view.topAnchor, leading: leadingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        paginationController.view.anchor(top: previewLabel.bottomAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
