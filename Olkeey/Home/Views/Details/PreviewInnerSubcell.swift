//
//  PreviewInnerSubcell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 26/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class PreviewInnerSubcell: UICollectionViewCell {
    
    let imageView = UIImageView(cornerRadius: 12)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
        imageView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
        layer.borderWidth = 0.5
        layer.borderColor = UIColor(white: 0.88, alpha: 1).cgColor
        layer.cornerRadius = 12

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
