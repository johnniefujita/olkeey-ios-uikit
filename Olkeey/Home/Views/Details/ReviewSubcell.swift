//
//  ReviewSubcell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 26/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class ReviewSubcell: UICollectionViewCell {
    
    let titleLabel = UILabel(text: "Review Title", font: .systemFont(ofSize: 18), color: .black)
    let authorLabel = UILabel(text: "Author", font: .systemFont(ofSize: 16), color: .gray)
    let starsLabel = UILabel(text: "Stars", font: .systemFont(ofSize: 14), color: .black)
    let bodyLabel = UILabel(text: "ReviewBody\nreview\nreview\n", font: .systemFont(ofSize: 16), color: .black, numberOfLines: 6)
    let starsStackView: UIStackView = {
        var arrangedSubviews = [UIView]()
        (0..<5).forEach({ (_) in
            let imageView = UIImageView()
            imageView.image = UIImage(named: "star")!.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.init(red: 232 / 255, green: 130 / 255, blue: 0 / 255, alpha: 1)
            imageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
            arrangedSubviews.append(imageView)
        })
        arrangedSubviews.append(UIView())
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 6
        layer.shadowOpacity = 0.2
        layer.cornerRadius = 12
        layer.shadowOffset = CGSize(width: 0, height: 0)
        
        let stackView = VerticalStackView(arrangedSubviews: [UIStackView(arrangedSubviews: [titleLabel, authorLabel], customSpacing: 8), starsStackView, bodyLabel], spacing: 12)
        
        titleLabel.setContentCompressionResistancePriority(.init(0), for: .horizontal)
        authorLabel.textAlignment = .right
        
        addSubview(stackView)
        stackView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: nil, leading: leadingAnchor, padding: .init(top: 20, left: 20, bottom: -20, right: 20))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
