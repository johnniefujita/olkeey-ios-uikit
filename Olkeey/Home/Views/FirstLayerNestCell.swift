//
//  GroupedNestedCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 24/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit




class FirstLayerNestCell: UICollectionViewCell {
    
    let titleLabel = UILabel(text: "App Section", font: UIFont.boldSystemFont(ofSize: 24), color: .black)
    
    let firstLayerNestedController = FirstLayerNestedController()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        addSubview(titleLabel)
        
        //interesting call .view
        addSubview(firstLayerNestedController.view)
        
        firstLayerNestedController.view.anchor(top: titleLabel.bottomAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
        titleLabel.anchor(top: topAnchor, trailing: trailingAnchor, bottom: nil, leading: leadingAnchor, padding: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
        
    }

}

