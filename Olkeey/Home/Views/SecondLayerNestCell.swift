//
//  SecondLayerNestCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 25/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit




class SecondLayerNestCell: UICollectionViewCell {
    
    let imageView = UIImageView(cornerRadius: 12)
    let nameLabel = UILabel(text: "App", font: .systemFont(ofSize: 20), color: .black)
    let companyLabel = UILabel(text: "CompanyLabel", font: .systemFont(ofSize: 14), color: .lightGray)
    let contractButton = UIButton(title: "contract")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contractButton.backgroundColor = UIColor(white: 0.95, alpha: 1)
        contractButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        contractButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        contractButton.layer.cornerRadius = 16
        contractButton.titleLabel?.font = .boldSystemFont(ofSize: 14)
        
        
        imageView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        
        let labelStack = UIStackView(arrangedSubviews: [nameLabel, companyLabel])
        labelStack.axis = .vertical
        
        
        let stackView = UIStackView(arrangedSubviews: [imageView, labelStack, contractButton])
        addSubview(stackView)
        stackView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
        stackView.spacing = 16
        stackView.alignment = .center
        
        
       
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

