//
//  HeaderFirstNestedCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 25/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

// THIS CELL HAVE THE LABEL CALL LABEL AND OUTDOOR IMAGE USUALLY USED FOR THE HIGHTLIGHTED

class HeaderFirstNestedCell: UICollectionViewCell {
    
    let publisherLabel = UILabel(text: "SocialMusic", font: .boldSystemFont(ofSize: 12), color: .black)
    
    let titleLabel = UILabel(text: "As mulheres bonitas estão aqui", font: .systemFont(ofSize: 24), color: .black)
    
    let imageView = UIImageView(cornerRadius: 8)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        imageView.layer.borderColor = UIColor(white: 0.80, alpha: 1).cgColor
        imageView.layer.borderWidth = 0.5
        titleLabel.numberOfLines = 2
        
        let stackView = VerticalStackView(arrangedSubviews: [publisherLabel, titleLabel, imageView])
        addSubview(stackView)
        stackView.spacing = 8
        
        stackView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor, padding: UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

