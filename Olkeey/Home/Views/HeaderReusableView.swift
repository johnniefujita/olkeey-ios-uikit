//
//  HeaderCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 25/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class HeaderReusableView: UICollectionReusableView {
    
    let headerSubviewController = HeaderSubviewController()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(headerSubviewController.view)
        headerSubviewController.view.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
