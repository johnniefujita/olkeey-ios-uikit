//
//  SecondLayerNestCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 24/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit
import SDWebImage

class FirstLayerNestedController: HorizontalPaginationController , UICollectionViewDelegateFlowLayout{
    
    fileprivate let cellId = "cellId"
    
    var homeModels: HomeModels?
    
    var didSelectHandler: ((FeedResult) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView?.register(SecondLayerNestCell.self, forCellWithReuseIdentifier: cellId)
        
        
        collectionView.contentInset = .init(top: 0, left: 16, bottom: 0, right: 16)
//        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
//            layout.scrollDirection = .horizontal
//        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let app = homeModels?.feed.results[indexPath.item] {
            didSelectHandler?(app)
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SecondLayerNestCell
        let app = homeModels?.feed.results[indexPath.item]
        cell.nameLabel.text = app?.name
        cell.companyLabel.text = app?.artistName
        cell.imageView.sd_setImage(with: URL(string: app?.artworkUrl100 ?? ""))

        return cell
    }
    
    let lineSpacing: CGFloat = 10
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeModels?.feed.results.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = (view.frame.height -  44) / 3
        
        return CGSize(width: view.frame.width - 48, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
    }
}
