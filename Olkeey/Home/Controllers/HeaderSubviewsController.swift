//
//  HeaderSubviewsController.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 25/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//
import SDWebImage
import UIKit

class HeaderSubviewController: HorizontalPaginationController, UICollectionViewDelegateFlowLayout {
    
    fileprivate let cellId = "cellId"
    
    var socialApps = [SocialApp]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .clear
        collectionView.register(HeaderFirstNestedCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.contentInset = .init(top: 0, left: 16, bottom: 0, right: 16)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 48, height: view.frame.height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return socialApps.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HeaderFirstNestedCell
        
        let app = self.socialApps[indexPath.item]
        cell.publisherLabel.text = app.name
        cell.publisherLabel.textColor = UIColor.init(red: 255 / 255, green: 68 / 255, blue: 12 / 255, alpha: 1)
        cell.titleLabel.text = app.tagline
        cell.imageView.sd_setImage(with: URL(string: app.imageUrl))
        return cell
    }
    
}
