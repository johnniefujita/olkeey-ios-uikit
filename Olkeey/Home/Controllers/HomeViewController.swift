//
//  Pamplhets.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 22/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class HomeViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    fileprivate let cellId = "cellId"
    fileprivate let headerId = "headerId"
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.color = .gray
        activityIndicatorView.startAnimating()
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        
        navigationItem.title = "Home"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        collectionView?.register(FirstLayerNestCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(HeaderReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        view.addSubview(activityIndicatorView)
        activityIndicatorView.anchor(top: collectionView.topAnchor, trailing: collectionView.trailingAnchor, bottom: collectionView.bottomAnchor, leading: collectionView.leadingAnchor)
        collectionView.alwaysBounceVertical = true
        fetchData()
    }
    
    
    // a variable to make the home models available whe the fetch is returned, so the values can be used
    // SUBSTITUED BY THE ARRAY var editorsChoiceGames: HomeModels?
    // This a multiple api request storage variable, so multiple sections can be fed
    var modelSections = [HomeModels]()
    var socialApps = [SocialApp]()
    
   
    
    fileprivate func fetchData() {
        
        // A section variable for a section returned from the api
        var section1: HomeModels?
        var section2: HomeModels?
        
        let dispatchGroup = DispatchGroup()
        
        
        dispatchGroup.enter()
        NetworkingService.shared.fetchTopFreeApps { (homeModels, error) in
            dispatchGroup.leave()
            section1 = homeModels
        }
    
        dispatchGroup.enter()
        NetworkingService.shared.fetchGames { (homeModels, error) in
            dispatchGroup.leave()
            section2 = homeModels
        }
        dispatchGroup.enter()
        NetworkingService.shared.fetchSocialApps { (apps, error) in
            dispatchGroup.leave()
            self.socialApps = apps ?? []
        }
        
        dispatchGroup.notify(queue: .main) {
            
            self.activityIndicatorView.stopAnimating()
            
            if let section = section1 {
                self.modelSections.append(section)
                print(section)
            }
            
            if let section = section2 {
                self.modelSections.append(section)
            }
            self.collectionView.reloadData()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! HeaderReusableView
        
        header.headerSubviewController.socialApps = self.socialApps
        header.headerSubviewController.collectionView.reloadData()
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 300)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FirstLayerNestCell
        let section = modelSections[indexPath.item]
        
        cell.titleLabel.text = section.feed.title
        cell.firstLayerNestedController.homeModels = section
        cell.firstLayerNestedController.collectionView.reloadData()
        
        cell.firstLayerNestedController.didSelectHandler = { [weak self] feedResult in
            
            
            let detailedController = DetailedViewController(appId: feedResult.id)
            detailedController.navigationItem.title = feedResult.name
            
                self?.navigationController?.pushViewController(detailedController, animated: true)
            
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return modelSections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    }
}
