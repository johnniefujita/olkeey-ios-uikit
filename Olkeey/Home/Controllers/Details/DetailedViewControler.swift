//
//  DetailedViewControler.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 26/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit

class DetailedViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    fileprivate let appId: String
    
    init(appId: String) {
        self.appId = appId
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate let cellId = "cellId"
    fileprivate let previewCellId = "previewCellId"
    fileprivate let reviewCellId = "reviewCellId"
    
    
    var app: Result?
    var reviews: Reviews?
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.tintColor = Config.themeColor
        
        collectionView.register(DetailCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(PreviewCollectionViewCell.self, forCellWithReuseIdentifier: previewCellId)
        collectionView.register(ReviewCollectionViewCell.self, forCellWithReuseIdentifier: reviewCellId)
        
        fetchData()
    }
    
    fileprivate func fetchData() {
        
        let urlString = "https://itunes.apple.com/lookup?id=\(appId)"
        NetworkingService.shared.fetchGenericJSONData(urlString: urlString) { (result: SearchResult?, err) in
            let app = result?.results.first
            self.app = app
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        // call for the endpoint the has the reviews
        let reviewsUrl = "https://itunes.apple.com/rss/customerreviews/page=1/id=\(appId)/sortby=mostrecent/json?l=en&cc=us"
        NetworkingService.shared.fetchGenericJSONData(urlString: reviewsUrl) {
            (reviews: Reviews?, err) in
            
            if let err = err {
                print("Error", err)
                return
            }
            
            self.reviews = reviews
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.item == 0 {
            let dummyCell = DetailCollectionViewCell(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 2000))
            dummyCell.app = app
            dummyCell.layoutIfNeeded()
            
            let estimatedSize = dummyCell.systemLayoutSizeFitting(CGSize(width: view.frame.width, height: 2000))
            
            return CGSize(width: view.frame.width, height: estimatedSize.height)
        }
        else if indexPath.item == 1 {
            return CGSize(width: view.frame.width, height: 500)
        }
        else {
            return CGSize(width: view.frame.width, height: 300)
        }
        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DetailCollectionViewCell
            
            // the type app that was created in the cell class now can receive the appData from the fetch function
            cell.app = app
            return cell
        }
        else if indexPath.item == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: previewCellId, for: indexPath) as! PreviewCollectionViewCell
            cell.paginationController.app = self.app
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reviewCellId, for: indexPath) as! ReviewCollectionViewCell
            cell.reviewSubcellsController.reviews = self.reviews
            return cell
        }
    }
}
