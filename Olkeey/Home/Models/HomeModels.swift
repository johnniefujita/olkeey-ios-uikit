//
//  AppSection.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 25/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import Foundation

// FOR THE SECTION
struct HomeModels: Decodable {
    let feed: Feed
}

struct Feed: Decodable {
    let title : String
    let results: [FeedResult]
}

struct FeedResult: Decodable {
    let id, name, artistName, artworkUrl100: String
    
}


// FOR THE HEADER OUTDORS

struct SocialApp: Decodable {
    let id, name, imageUrl, tagline: String
}


// REVIEWS
struct Reviews: Decodable {
    let feed: ReviewFeed
}

struct ReviewFeed: Decodable {
    let entry: [Entry]
}

struct Entry: Decodable {
    let author: Author
    let title: Label
    let id: Label
    let content: Label
    
    let rating: Label
    
    private enum CodingKeys: String, CodingKey {
        case author, title, content, id
        case rating = "im:rating"
    }
}

struct Label: Decodable {
    let label: String
}

struct Author: Decodable {
    let name: Label
}



