//
//  NetworkingServices.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 23/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import Foundation


class NetworkingService {
    
    static let shared = NetworkingService() // Singleton
    
    func fetchApps(searchTerm: String, completion: @escaping (SearchResult?, Error?) -> ()) {
        let urlString = "https://itunes.apple.com/search?term=\(searchTerm)&entity=software"
        
        fetchGenericJSONData(urlString: urlString, completion: completion)
        
    }
    
    
    
    func fetchTopFreeApps(completion: @escaping (HomeModels?, Error?) -> ()) {
       let urlString = "https://rss.itunes.apple.com/api/v1/us/ios-apps/top-free/all/50/explicit.json"
        fetchSection(urlString: urlString, completion: completion)
    }
    
    func fetchGames(completion: @escaping (HomeModels?, Error?) -> ()) {
        let urlString = "https://rss.itunes.apple.com/api/v1/us/ios-apps/new-games-we-love/all/50/explicit.json"
        fetchSection(urlString: urlString, completion: completion)
    }
    
    func fetchSection(urlString: String, completion: @escaping (HomeModels?, Error?) -> Void) {
        fetchGenericJSONData(urlString: urlString, completion: completion)
    }
    
    func fetchSocialApps(completion: @escaping ([SocialApp]?, Error?) -> Void) {
        let urlString = "https://api.letsbuildthatapp.com/appstore/social"
        fetchGenericJSONData(urlString: urlString, completion: completion)
    }
    
    func fetchGenericJSONData<T: Decodable>(urlString: String, completion: @escaping (T?, Error?) -> ()) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            do {
                let objects = try JSONDecoder().decode(T.self, from: data!)
                completion(objects, nil)
            } catch let jsonError {
                completion(nil, jsonError)
                return
            }
        }.resume()
        
    }
}
