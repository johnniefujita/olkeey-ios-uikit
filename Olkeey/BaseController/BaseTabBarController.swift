//
//  BaseTabBarController.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 22/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit
import SDWebImage
class BaseTabBarController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let todayNavigationController = UINavigationController(rootViewController: TodayCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout()))
        todayNavigationController.tabBarItem.title = "Today"
        todayNavigationController.tabBarItem.image = UIImage(named: "today")
        
        let homeNavigationViewController = UINavigationController(rootViewController: HomeViewController(collectionViewLayout: UICollectionViewFlowLayout()))
        
        homeNavigationViewController.tabBarItem.title = "Home"
        homeNavigationViewController.tabBarItem.image = UIImage(named: "home-page")
        
        let searchNavigationViewController = UINavigationController(rootViewController: SearchViewController(collectionViewLayout: UICollectionViewFlowLayout()))
        searchNavigationViewController.tabBarItem.title = "Search"
        searchNavigationViewController.tabBarItem.image = UIImage(named:"keey")
        
        tabBar.tintColor = .orange
        
        viewControllers = [todayNavigationController, homeNavigationViewController, searchNavigationViewController]
    }
    
    fileprivate func createNavController(viewController: UIViewController, title: String, imageName: String) -> UIViewController {
        
        let navController = UINavigationController(rootViewController: viewController)
        
        navController.tabBarItem.title = title
        navController.tabBarItem.image = UIImage(named: imageName)
        
        return navController
    }
}
