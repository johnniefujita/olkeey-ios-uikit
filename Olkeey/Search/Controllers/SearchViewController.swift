//
//  SearchViewController.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 22/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit
import SDWebImage

class SearchViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    fileprivate let cellId = "cellId"
    
    // a view that manages the result of a searchfield
    
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    
    fileprivate let enterSearchTermLabel: UILabel = {
       let label = UILabel()
        label.text = "Que é que ce ta atras dez ano??"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .gray
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.init(white: 0.98, alpha: 1)
        navigationBarConfig()
        
        collectionView?.register(SearchResultCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView?.addSubview(enterSearchTermLabel)
        enterSearchTermLabel.anchor(top: collectionView?.topAnchor, trailing: collectionView?.trailingAnchor, bottom: collectionView?.bottomAnchor, leading: collectionView?.leadingAnchor, padding: UIEdgeInsets(top: view.frame.height / 4, left: view.frame.width / 8, bottom: 0, right: 0))
        
        
        setupSearchBar()
    }
    
    fileprivate func setupSearchBar() {
        definesPresentationContext = true
        navigationItem.searchController = self.searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        
    }
    
    
    //TIMER FOR THROTTLING
    var timer: Timer?
    // listener to the searchbar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false, block: {
            (_) in
            
            NetworkingService.shared.fetchApps(searchTerm: searchText) {
                (res, err)
                in
                if let err = err {
                    print("failed to fetch App, returning error", err)
                    return
                }
                self.appResults = res?.results ?? []
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        })
    }
    
    
    fileprivate var appResults = [Result]()
    
    
    fileprivate func fetchApps() {
        NetworkingService.shared.fetchApps(searchTerm: "") { (results, error) in
            
            if let error = error {
                print("Failed Fetchin apps:", error)
                return
            }
            
            self.appResults = results?.results ?? []
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func navigationBarConfig() {
        navigationItem.title = "Search"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let appId = appResults[indexPath.item].trackId
        let detailedViewController = DetailedViewController(appId: String(appId))
        navigationController?.pushViewController(detailedViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 350)
    }
    // collection views uses indexPath.item and table views use indexPath.row
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SearchResultCell
        cell.appResult = appResults[indexPath.item]
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        enterSearchTermLabel.isHidden = appResults.count != 0
        
        return appResults.count
    }
    
}
