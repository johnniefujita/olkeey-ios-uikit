//
//  SearchResultCell.swift
//  AppStoreClone
//
//  Created by johnnie fujita on 22/09/19.
//  Copyright © 2019 johnnie fujita. All rights reserved.
//

import UIKit
import SDWebImage

class SearchResultCell: UICollectionViewCell {
    
    var appResult: Result! {
        didSet {
            nameLabel.text = appResult.trackName
            categoryLabel.text = appResult.primaryGenreName
            ratingLabel.text = "\(appResult.averageUserRating ?? 0)"
            
            let urlartWork = URL(string: appResult.artworkUrl100)
            imageView.sd_setImage(with: urlartWork)
            
            screenShotImageView.sd_setImage(with: URL(string: appResult.screenshotUrls[0]))
            
            if appResult.screenshotUrls.count > 1 {
                screenShotImageView2.sd_setImage(with: URL(string: appResult.screenshotUrls[1]))
            }
            
            if appResult.screenshotUrls.count > 2 {
                screenShotImageView3.sd_setImage(with: URL(string: appResult.screenshotUrls[2]))
            }
        }
    }
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        // imageView.backgroundColor = .lightGray
        imageView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 64).isActive = true
        imageView.layer.cornerRadius = 12
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let nameLabel: UILabel = {
        
        let label = UILabel()
        label.text = "NAME"
        return label
    }()
    
    let categoryLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Genre"
        return label
    }()
    
    let ratingLabel: UILabel = {
        
        let label = UILabel()
        label.text = "9.8"
        return label
    }()
    
    let getButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Contract", for: .normal)
        button.setTitleColor(.orange, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.backgroundColor = UIColor(white: 0.95, alpha: 1)
        button.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        button.layer.cornerRadius = 12
        return button
    }()

    
    lazy var screenShotImageView = self.createScreenshotImageView()
    lazy var screenShotImageView2 = self.createScreenshotImageView()
    lazy var screenShotImageView3 = self.createScreenshotImageView()
    
    func createScreenshotImageView() -> UIImageView {
        let imageView = UIImageView()
        // imageView.backgroundColor = .lightGray
        imageView.layer.cornerRadius = 12
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.init(white: 0.80, alpha: 1).cgColor
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        
        // The labels stack view
        let labelsStackView = VerticalStackView(arrangedSubviews: [nameLabel, categoryLabel, ratingLabel])
        labelsStackView.alignment = .leading
        labelsStackView.distribution = .fillEqually
        
        // The upper stackView that both contains the labels, the button to acquire and the image
        let infoStackView = UIStackView(arrangedSubviews: [imageView, labelsStackView, getButton])
        infoStackView.spacing = 12
        infoStackView.alignment = .center
        
        
        // Creates the botom stack view that holds the screenshots
         let screenShotsStackView = UIStackView(arrangedSubviews: [screenShotImageView, screenShotImageView2, screenShotImageView3])
        screenShotsStackView.spacing = 12
        screenShotsStackView.distribution = .fillEqually
        
        // creates the container stack that compose the total cell
        let containerStackView = VerticalStackView(arrangedSubviews: [infoStackView, screenShotsStackView], spacing: 16)
        
        addSubview(containerStackView)
        
        
       
        
        // anchor the CONTAINER TO THE CELL WITH SOME MARGINS
        containerStackView.anchor(top: topAnchor, trailing: trailingAnchor, bottom: bottomAnchor, leading: leadingAnchor, padding: UIEdgeInsets(top: 16, left: 16, bottom: -16, right: 16))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIView {
    
    func anchorSize(to view: UIView, multiplier: CGFloat) {
        widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: multiplier).isActive = true
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}

